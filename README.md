TimeAtlas
=========

A toolbox to handle, analyze and make predictions with time series data.
    

Develop
-------

To develop this package. Execute the following command from the root of the project

    pipenv install
    
Please add an example of the method(s) you did develop in `docs/getting_started.ipynb`.


Author
------

Frédéric Montet
frederic.montet@hefr.ch

Lorenz Rychener
lorenz.rychener@hefr.ch