User Guide
==========

.. toctree::
    0_time-series
    1_metadata
    2_models
    3_generators
