Types
=====

.. autoclass:: timeatlas.types.Sensor
    :members:

.. autoclass:: timeatlas.types.Unit
    :members: