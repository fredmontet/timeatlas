API Reference
=============

.. toctree::
    abstract
    generators
    io
    metadata
    models
    time_series
    time_series_dataset
    types
