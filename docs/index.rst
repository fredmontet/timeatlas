Welcome to TimeAtlas's documentation!
=====================================


.. toctree::
   getting_started/index
   user_guide/index
   api_reference/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
