# Generic
DEFAULT_EXPORT_FILENAME = "export"

# Metadata
METADATA_FILENAME = "meta"
METADATA_EXT = "json"

# TimeSeries
TIME_SERIES_FILENAME = "data"
TIME_SERIES_EXT = "csv"

# Misc. IO
PICKLE_EXT = "pkl"
